from django.urls import path
from . import views


urlpatterns = {
    path('', views.employed, name='employed'),
    path('send_email_employed/', views.send_email_employed, name='send-email-employed'),
}