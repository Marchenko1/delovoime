from django.conf import settings
from django.http import HttpResponse
from django.shortcuts import render
from django.core.mail import send_mail


def employed(request):
    return render(request, 'employed/index.html', {})


def send_email_employed(request):
    name = request.POST.get('name', '')
    phone = request.POST.get('phone', '')
    city = request.POST.get('city', '')
    district = request.POST.get('district', '')
    send_mail(
        'Самозанятый',
        f'Имя: {name}\nТелефон: {phone}\nГород: {city}\nРайон: {district}\nПросит обратную связь!!!',
        settings.EMAIL_HOST_USER,
        ['mailbox@delovoi.me'],
    )
    print(name)
    print(phone)
    print(city)
    print(district)
    return HttpResponse(status=200)
