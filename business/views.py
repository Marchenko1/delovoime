from django.conf import settings
from django.http import HttpResponse
from django.shortcuts import render
from django.core.mail import send_mail


def business(request):
    return render(request, 'business/index.html', {})


def send_email_business(request):
    name = request.POST.get('name', '')
    phone = request.POST.get('phone', '')
    company = request.POST.get('company', '')
    send_mail(
        'Бизнес',
        f'Имя: {name}\nТелефон: {phone}\nКомпания: {company}\nПросят обратную связь!!!',
        settings.EMAIL_HOST_USER,
        ['mailbox@delovoi.me'],
    )
    return HttpResponse(status=200)
