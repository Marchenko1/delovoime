from django.urls import path
from . import views


urlpatterns = [
    path('', views.business, name='business'),
    path('send_email/', views.send_email_business, name='send-email'),
]