$(document).ready(function ($) {
    $.ajaxSetup({
        beforeSend: function (xhr, settings) {
            function getCookie(name) {
                var cookieValue = null;
                if (document.cookie && document.cookie != '') {
                    var cookies = document.cookie.split(';');
                    for (var i = 0; i < cookies.length; i++) {
                        var cookie = jQuery.trim(cookies[i]);
                        // Does this cookie string begin with the name we want?
                        if (cookie.substring(0, name.length + 1) == (name + '=')) {
                            cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                            break;
                        }
                    }
                }
                return cookieValue;
            }

            if (!(/^http:.*/.test(settings.url) || /^https:.*/.test(settings.url))) {
                // Only send the token to relative URLs i.e. locally.
                xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
            }
        }
    });

    $('.popup-open').click(function () {
        $('.popup-fade').fadeIn();
        return false;
    });

    $('#id_phone').mask('+7 (999) 999-99-99');


    $('.send-popup').click(function () {
        if ($('#id_phone').val() == '') {
            alert('Не заполнен телефон!');
        }
        if ($('#id_name').val() == '') {
            alert('Не заполнено имя!');
        }
        if ($('#id_company').val() == '') {
            alert('Не заполнено поле "Название компании"!');
        } else {
            let phone = $('#id_phone').val();
            let name = $('#id_name').val();
            let company = $('#id_company').val();
            $.ajax({
                url: '/send_email/',
                type: 'POST',
                data: {
                    'phone': phone,
                    'name': name,
                    'company': company,
                },
                // dataType: 'json',
                success: function (json) {
                    alert('Мы свяжемся с вами в ближайшее время!');
                    $('.popup-close').trigger("click");
                    console.log('+')
                }
            });
        }
    });


    $('.popup-close').click(function () {
        $(this).parents('.popup-fade').fadeOut();
        return false;
    });

    $(document).keydown(function (e) {
        if (e.keyCode === 27) {
            e.stopPropagation();
            $('.popup-fade').fadeOut();
        }
    });

    $('.popup-fade').click(function (e) {
        if ($(e.target).closest('.popup').length == 0) {
            $(this).fadeOut();
        }
    });
});