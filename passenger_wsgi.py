# -*- coding: utf-8 -*-
import os, sys
sys.path.insert(0, '/home/django/delovoime')
sys.path.insert(1, '/home/django/delovoime/.venv/lib/python3.8/site-packages')
os.environ['DJANGO_SETTINGS_MODULE'] = 'delovoime.settings'
from django.core.wsgi import get_wsgi_application
application = get_wsgi_application()